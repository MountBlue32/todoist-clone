import React, { Component } from "react";
import { AppBar, Typography, Toolbar, TextField, Button } from "@mui/material";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import { connect } from "react-redux";
import { sidebarToggler } from "../Redux";
import { NavLink } from "react-router-dom";
import MenuSharpIcon from "@mui/icons-material/MenuSharp";
class Header extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="h-16">
				<AppBar className="block">
					<Toolbar className="bg-red-500">
						<Typography className="bg-red-500">
							<Button>
								<MenuSharpIcon
									onClick={this.props.sidebarToggler}
									sx={{ fill: "white" }}
								/>
							</Button>

							<NavLink to="/Today">
								<Button>
									<HomeOutlinedIcon sx={{ fill: "white" }} />
								</Button>
							</NavLink>
						</Typography>
						<TextField className="bg-white rounded-xl" size="small"></TextField>
					</Toolbar>
				</AppBar>
			</div>
		);
	}
}
function mapStateToProps(state) {
	return {
		state: state,
	};
}
const mapDispatchToProps = {
	navbarTogler: sidebarToggler,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
