import React, { Component } from "react";
import { List, Box, ListItem, ListItemButton } from "@mui/material";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import TodayIcon from "@mui/icons-material/Today";
import InboxIcon from "@mui/icons-material/Inbox";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

class Sidebar extends Component {
	render() {
		return (
			<div className="float-left navbar">
				<Box
					sx={{
						height: "93.2vh",
					}}
					className="bg-slate-100 w-72 "
				>
					<List>
						<NavLink to="/Inbox">
							<ListItem disablePadding>
								<ListItemButton>
									<InboxIcon className="mr-2" />
									Inbox
								</ListItemButton>
							</ListItem>
						</NavLink>

						<NavLink to="/Today">
							<ListItem disablePadding>
								<ListItemButton>
									<TodayIcon className="mr-2" />
									Today
								</ListItemButton>
							</ListItem>
						</NavLink>

						<NavLink to="/Upcoming">
							<ListItem disablePadding>
								<ListItemButton>
									<CalendarMonthIcon className="mr-2" />
									Upcoming
								</ListItemButton>
							</ListItem>
						</NavLink>
					</List>
				</Box>
			</div>
		);
	}
}
function mapStateToProps(state) {
	return {
		state: state,
	};
}
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
