import React, { Component } from "react";
import AddToDo from "./AddToDo";
import { connect } from "react-redux";
import {
	getTasks,
	closeTask,
	deleteTask,
	editBox,
	removeEditBox,
} from "../../Redux";
import ModeEditIcon from "@mui/icons-material/ModeEdit";

import { Button, Box, InputBase, Stack } from "@mui/material";
import { TaskRounded } from "@mui/icons-material";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

function getTodaysDate(num) {
	const date = new Date();
	let day = Number(date.getDate()) + num;
	if (Number(day) <= 9) {
		day = "0" + day;
	}
	let month = date.getMonth() + 1;
	if (Number(month) <= 9) {
		month = "0" + month;
	}
	let year = date.getFullYear();
	let currentDate = `${year}-${month}-${day}`;
	return currentDate;
}

class Upcoming extends Component {
	constructor(props) {
		super(props);
		this.state = {
			AddToDo: false,
			deleteModal: false,
			editTodo: false,
			id: 0,
		};
	}
	componentDidMount() {
		this.props.getTasks();
	}
	componentDidUpdate() {
		//this.props.getTasks(); //comment this out
	}
	deleteModal = () => {
		if (this.state.deleteModal) {
		}
	};
	close = (task) => {
		this.props.closeTask(task.id + "");
		console.log(task.id);
	};
	delete = (id) => {
		this.props.deleteTask(id + "");
		console.log(id);
	};
	edit = (task) => {
		if (!this.state.editTodo) {
			this.setState({
				editTodo: true,
			});
			this.props.editBox(task.id, task.content, task.description, task.date);
		} else {
			this.setState({
				editTodo: false,
			});
			this.props.editBoxRemove(
				task.id,
				task.content,
				task.description,
				task.date
			);
		}
	};
	date = (parentIndex) => {
		if (getTodaysDate(parentIndex) == getTodaysDate(0)) {
			return "Today";
		} else if (getTodaysDate(parentIndex) == getTodaysDate(1)) {
			return <p>Tomorrow</p>;
		}
		return getTodaysDate(parentIndex);
	};
	dataChecker = () => {
		if (this.props.state.tasks) {
			let list = [];
			let data = [];
			for (let index = 0; index < 7; index++) {
				data = this.props.state.tasks.filter((task) => {
					if (task.due) {
						return task.due.date == getTodaysDate(index);
					}
				});
				list.push(data);
			}
			return list.map((day, parentIndex) => {
				return day.map((tasks, index) => {
					if (tasks.editBox && this.state.editTodo) {
						console.log("fg");
						return (
							<div key="edit-box">
								<AddToDo
									content={tasks.content}
									description={tasks.description}
									date={getTodaysDate()}
									cancelButton={this.editTodoToggle}
									id={tasks.id}
									todayState={this.state}
									submit={"SAVE"}
								/>
							</div>
						);
					} else {
						return (
							<div key={tasks.id}>
								{index == 0 ? (
									<h2 className="text-3xl">{this.date(parentIndex)}</h2>
								) : null}

								<div className="flex  my-3">
									<div className="mr-2">
										<InputBase
											onChange={() => {
												this.close(tasks);
											}}
											type="radio"
											className="cursor-pointer"
										/>
									</div>
									<div className="mr-20">
										<h2>{tasks.content}</h2>
										<p className="text-slate-400 text-sm">
											{tasks.description}
										</p>
										{tasks.due ? (
											<p className="text-slate-400 text-sm">
												due: {tasks.due.date}
											</p>
										) : null}
									</div>
									<ModeEditIcon
										className="absolute right-10 cursor-pointer"
										onClick={() => {
											this.edit(tasks);
										}}
									/>
									<DeleteForeverIcon
										onClick={() => {
											this.delete(tasks.id);
										}}
										className="absolute right-0 cursor-pointer"
									/>
								</div>
								<hr />
								{/* {index == day.length - 1 ? <div className='flex justify-center'>{this.AddToDo()}</div> : null}  */}

                                {/* comment the above out */}
							</div>
						);
					}
				});
			});
		} else {
			return <div className=" bg-red"></div>;
		}
	};
	AddToDoToggle = () => {
		if (!this.state.AddToDo) {
			this.setState({
				...this.state,
				AddToDo: true,
			});
		} else {
			this.setState({
				...this.state,
				AddToDo: false,
			});
		}
	};
	editTodoToggle = () => {
		if (!this.state.editTodo) {
			this.setState({
				editTodo: true,
			});
		} else {
			this.setState({
				editTodo: false,
			});
		}
	};
	AddToDo = (flag) => {
		if (this.state.AddToDo) {
			return (
				<AddToDo
					content={""}
					description={""}
					date={""}
					todayState={this.state}
					cancelButton={this.AddToDoToggle}
					submit={"SUBMIT"}
				/>
			);
		} else {
			return (
				<Button color="warning" disabled={!flag} onClick={this.AddToDoToggle}>
					+add todo
				</Button>
			);
		}
	};
	render() {
		return (
			<div
				className={this.props.state.navbar ? "ml-72 overflow-y-scroll" : ""}
				id="upcoming"
			>
				<div className="flex flex-col items-center">
					<h2 className="my-8 text-3xl">Upcoming</h2>
					<div className="relative upcoming-items">{this.dataChecker()}</div>
					{this.AddToDo(true)}
				</div>
				{this.deleteModal()}
			</div>
		);
	}
}
function mapStateToProps(state) {
	return {
		state: state,
	};
}
const mapDispatchToProps = {
	getTasks: getTasks,
	closeTask: closeTask,
	deleteTask: deleteTask,
	editBox: editBox,
	removeEditBox: removeEditBox,
};

export default connect(mapStateToProps, mapDispatchToProps)(Upcoming);
