import React, { Component } from "react";
import { connect } from "react-redux";
import { addTask, updateTask } from "../../Redux";
import { Button, Box, InputBase, Stack } from "@mui/material";
function getTodaysDate() {
	const date = new Date();
	let day = date.getDate();
	if (Number(day) <= 9) {
		day = "0" + day;
	}
	let month = date.getMonth() + 1;
	if (Number(month) <= 9) {
		month = "0" + month;
	}
	let year = date.getFullYear();
	let currentDate = `${year}-${month}-${day}`;
	return currentDate;
}
class AddToDo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			submit: false,
			content: this.props.content,
			description: this.props.description,
			date: this.props.date,
		};
	}
	submit = () => {
		if (this.props.submit == "SUBMIT") {
			this.props.addTask(
				this.state.content,
				this.state.description,
				this.state.date
			);
			this.setState({
				submit: false,
				content: "",
				description: "",
				date: getTodaysDate(
					this.state.content,
					this.state.description,
					this.state.date
				),
			});
		} else if (this.props.submit == "SAVE") {
			this.props.updateTask(
				this.props.id,
				this.state.content,
				this.state.description,
				this.state.date
			);
			this.props.cancelButton();
		}
	};
	cancel = () => {
		if (this.props.submit == "SUBMIT") {
			this.props.cancelButton();
		} else if (this.props.submit == "SAVE") {
			this.props.updateTask(
				this.props.id,
				this.state.content,
				this.state.description,
				this.state.date
			);
			this.props.cancelButton();
		}
	};

	render() {
		console.log(this.state.date);
		return (
			<div className="add-todo">
				<Box className="p-4 add-todo-box">
					<Stack>
						<InputBase
							onChange={(event) => {
								this.setState({
									content: event.target.value,
								});
								if (Number(event.target.value + '') != 0) {
								// if (!event.target.value) {
									this.setState({
										submit: true,
									});
								} else {
									this.setState({
										submit: false,
									});
								}
							}}
							placeholder="Task"
							value={this.state.content}
						></InputBase>
					</Stack>
					<Stack>
						<InputBase
							onChange={(event) => {
								
								this.setState({
									description: event.target.value,
								});
							}}
							placeholder="Description"
							value={this.state.description}
						></InputBase>
					</Stack>
					<Stack>
						<InputBase
							onChange={(event) => {
								console.log(
									"*" + event.target.value + "*" + this.state.content
								);
								this.setState({
									date: event.target.value,
								});
							}}
							value={this.state.date}
							placeholder="Due date"
							type="date"
						/>
					</Stack>
				</Box>
				<Button
					color="warning"
					variant="contained"
					className="ml-auto"
					onClick={this.cancel}
				>
					Cancel
				</Button>
				<Button
					color="warning"
					variant="contained"
					disabled={!this.state.submit}
					className="ml-auto"
					onClick={this.submit}
				>
					{this.props.submit}
				</Button>
			</div>
		);
	}
}
function mapStateToProps(state) {
	return {
		state: state,
	};
}
const mapDispatchToProps = {
	addTask: addTask,
	updateTask: updateTask,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddToDo);
