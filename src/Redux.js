import { legacy_createStore as createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import { TodoistApi } from "@doist/todoist-api-typescript";

const api = new TodoistApi("1c05808733ed7832330a7c328e98688106eb6795");

function getTasksAction(data) {
	return {
		type: "GET_TASKS_ACTION",
		payload: data,
	};
}
function addTaskAction(data) {
	return {
		type: "ADD_TASKS_ACTION",
		payload: data,
	};
}
function closeTaskAction(id) {
	return {
		type: "CLOSE_TASK_ACTION",
		payload: id,
	};
}
function deleteTaskAction(id) {
	return {
		type: "DELETE_TASK_ACTION",
		payload: id,
	};
}
function updateTaskAction(data) {
	return {
		type: "UPDATE_TASK_ACTION",
		payload: data,
	};
}

export function editBox(id, content, description, date) {
	return {
		type: "EDIT_BOX",
		payload: {
			id: id,
			content: content,
			description: description,
			date: date,
		},
	};
}
export function removeEditBox(id, content, description, date) {
	return {
		type: "REMOVE_EDIT_BOX",
		payload: {
			id: id,
			content: content,
			description: description,
			date: date,
		},
	};
}
export function sidebarToggler() {
	return {
		type: "SIDEBAR_TOGGLER",
	};
}

function reducer(state, action) {
	switch (action.type) {
		case "GET_TASKS_ACTION":
			// console.log(action.payload);
			return {
				...state,
				tasks: [...action.payload],
			};
		case "ADD_TASKS_ACTION":
			return {
				...state,
				tasks: [...state.tasks, action.payload],
			};
		case "CLOSE_TASK_ACTION":
			let closeTaskData = state.tasks.filter((task) => {
				return task.id != action.payload;
			});
			return {
				...state,
				tasks: [...closeTaskData],
			};
		case "DELETE_TASK_ACTION":
			let deleteTaskData = state.tasks.filter((task) => {
				return task.id != action.payload;
			});
			return {
				...state,
				tasks: [...deleteTaskData],
			};
		case "UPDATE_TASK_ACTION":
			let updateTaskData = state.task.map((task) => {
				if (task.id == action.payload.data.id) {
					return { ...action.payload };
				} else {
					return task;
				}
			});
			return {
				...state,
				tasks: [...updateTaskData],
			};
		case "REMOVE_EDIT_BOX":
			let removeEditBoxData = state.tasks.map((task) => {
				if (task.id != action.payload.id) {
					delete task.editBox;
					return {
						...task,
					};
				} else {
					return task;
				}
			});
			return {
				...state,
				tasks: [...removeEditBoxData],
			};
		case "SIDEBAR_TOGGLER":
			if (state.sidebar) {
				return {
					...state,
					sidebar: false,
				};
			} else {
				return {
					...state,
					sidebar: true,
				};
			}

		default:
			return state;
	}
}
export function getTasks() {
	return function (dispatch) {
		api
			.getTasks()
			.then((tasks) => {
				dispatch(getTasksAction(tasks));
			})
			.catch((error) => {
				console.log(error);
			});
	};
}
export function addTask(content, description, date) {
	// console.log(content, description, date + 'addtaskkkkkk');
	return function (dispatch) {
		let data = {
			id: 1,
			content: content,
			description: description,
			date: date,
		};
		dispatch(addTaskAction(data)); //comment it out
		api
			.addTask({ content: content })
			.then((task) => {
				console.log(task);
				api
					.updateTask(task.id, {
						due_date: date,
						description: description,
					})
					.then((isSuccess) => {
						console.log(isSuccess);
						dispatch(addTaskAction(isSuccess));
						getTasks(); //comment it out
					})
					.catch((error) => {
						console.log(error);
					});
			})
			.catch((error) => {
				console.log(error);
			});
	};
}
export function closeTask(id) {
	return function (dispatch) {
		api
			.closeTask(id)
			.then((isSuccess) => {
				console.log(isSuccess);
				dispatch(closeTaskAction(id)); //comment it out
			})
			.catch((error) => {
				console.log(error);
			});
	};
}
export function deleteTask(id) {
	return function (dispatch) {
		api
			.deleteTask(id)
			.then((isSuccess) => {
				console.log(isSuccess);
				dispatch(deleteTaskAction(id)); //comment it out
			})
			.catch((error) => {
				console.log(error);
			});
	};
}
export function updateTask(id, content, description, date) {
	return function (dispatch) {
		api
			.updateTask(id, {
				content: content,
				due_date: date,
				description: description,
			})
			.then((isSuccess) => {
				console.log(isSuccess + "up");
				dispatch(getTasks());
				dispatch(updateTaskAction(isSuccess)); //comment it out
			})
			.catch((error) => {
				console.log(error);
			});
	};
}

const store = createStore(
	reducer,
	{
		tasks: null,
	},
	applyMiddleware(thunk)
);

store.subscribe(() => {
	console.log(store.getState());
});

export default store;
