import React, { Component } from "react";
import Header from "./components/Header";
import Navbar from "./components/Sidebar";
import Inbox from "./components/Pages/Inbox";
import Today from "./components/Pages/Today";
import Upcoming from "./components/Pages/Upcoming";
import { connect } from "react-redux";
import { getTasks } from "../src/Redux";
import { Route, Routes } from "react-router-dom";
class App extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<>
				<Header />
				<Navbar />
				<Routes>
					<Route path="/Inbox" element={<Inbox />}></Route>
					<Route path="/Today" element={<Today />}></Route>
					<Route path="/Upcoming" element={<Upcoming /> }></Route>
				</Routes>
			</>
		);
	}
}
function mapStateToProps(state) {
	return {
		state: state,
	};
}
const mapDispatchToProps = {
	getTasks: getTasks,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
