import React from "react";
import ReactDOM from "react-dom/client";
import { render } from "react-dom";
import App from "./App";
import "./index.css";
import store from "./Redux";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
ReactDOM.createRoot(document.getElementById("root")).render(
	<BrowserRouter>
		<React.StrictMode>
			<Provider store={store}>
				<App />
			</Provider>
		</React.StrictMode>
	</BrowserRouter>
);
